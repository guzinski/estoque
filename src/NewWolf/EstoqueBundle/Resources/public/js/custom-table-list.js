function desmarcaSelectAll() {
    if (!this.checked) {
        jQuery('#select-all').each(function () {
            this.checked = false;
        });
    }
}

jQuery(document).ready(function(){
    // DataTables Length to Select2
    jQuery('div.dataTables_length select').removeClass('form-control input-sm');
    jQuery('div.dataTables_length select').css({width: '60px'});
    jQuery('div.dataTables_length select').select2({
        minimumResultsForSearch: -1
    });

    jQuery(".dataTables_wrapper  .row:first-child").append("<div class='col-xs-6'>"+jQuery(".dataTables_wrapper  .row:first-child .col-xs-6:first-child").html()+"</div>");
    jQuery(".dataTables_wrapper  .row:first-child .col-xs-6:first-child").remove();

    jQuery('#select-all').change(function() {   
        if(this.checked) {
            jQuery('table tbody :checkbox').each(function() {
                this.checked = true;                        
            });
        } else {
            jQuery('table tbody :checkbox').each(function() {
                this.checked = false;                        
            });
        }
    });

});
