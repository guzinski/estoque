<?php

namespace NewWolf\EstoqueBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * 
 * @ORM\Table(name="fornecedor")
 * @ORM\Entity(repositoryClass="NewWolf\EstoqueBundle\Repository\FornecedorRepository")
 * @author Luciano
 */
class Fornecedor extends BaseEntity
{
    
    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=150, nullable=false)
     */
    private $nome;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="fornecedores")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario", referencedColumnName="id")
     * })
     */
    private $usuario;
    
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Produto", mappedBy="fornecedor")
     **/
    private $produtos;
    
    
    public function __construct($nome = NULL)
    {
        $this->setNome($nome);
        $this->setDataCadastro(new \DateTime());
        $this->setProdutos(new ArrayCollection());
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getUsuario()
    {
        return $this->usuario;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function setUsuario(Usuario $usuario)
    {
        $this->usuario = $usuario;
        return $this;
    }

        
    public function getLabel()
    {
        return $this->nome;
    }
    
    public function getProdutos()
    {
        return $this->produtos;
    }

    public function setProdutos(Collection $produtos)
    {
        $this->produtos = $produtos;
        return $this;
    }



}
