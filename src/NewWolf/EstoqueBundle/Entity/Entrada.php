<?php

namespace NewWolf\EstoqueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * 
 * @ORM\Table(name="entrada")
 * @ORM\Entity(repositoryClass="NewWolf\EstoqueBundle\Repository\EntradaRepository")
 * @author Luciano
 */
class Entrada extends BaseEntity
{
        
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="datetime", nullable=false)
     */
    private $data;
    
    /**
     * @var float
     *
     * @ORM\Column(name="valor_unitario", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorUnitario;
    
    /**
     * @var int
     *
     * @ORM\Column(name="quantidade", type="integer", nullable=false)
     */
    private $quantidade;

    /**
     * @var Produto
     *
     * @ORM\ManyToOne(targetEntity="Produto", inversedBy="entradas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="produto", referencedColumnName="id")
     * })
     */
    private $produto;
    
    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="entradas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario", referencedColumnName="id")
     * })
     */
    private $usuario;

    
    public function __construct()
    {
        $this->setDataCadastro(new \DateTime());
    }
    

    public function getData()
    {
        return $this->data;
    }

    public function getQuantidade()
    {
        return $this->quantidade;
    }

    public function getProduto()
    {
        return $this->produto;
    }

    public function setData(\DateTime $data)
    {
        $this->data = $data;
        return $this;
    }

    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
        return $this;
    }

    public function setProduto(Produto $produto)
    {
        $this->produto = $produto;
        return $this;
    }
    
    public function getUsuario()
    {
        return $this->usuario;
    }

    public function setUsuario(Usuario $usuario)
    {
        $this->usuario = $usuario;
        return $this;
    }

    public function getLabel()
    {
        $this->getProduto()->getLabel();
    }

    public function getValorUnitario()
    {
        return $this->valorUnitario;
    }

    public function setValorUnitario($valorUnitario)
    {
        $this->valorUnitario = $valorUnitario;
        return $this;
    }


        
}
