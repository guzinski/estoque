<?php

namespace NewWolf\EstoqueBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Description of Cliente
 *
 * @ORM\Table(name="cliente")
 * @ORM\Entity(repositoryClass="NewWolf\EstoqueBundle\Repository\ClienteRepository")
 * @author Luciano
 */
class Cliente extends BaseEntity 
{
    
    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=150, nullable=false)
     */
    private $nome;
    
    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="clientes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario", referencedColumnName="id")
     * })
     */
    private $usuario;
    
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Saida", mappedBy="cliente")
     **/
    private $saidas;

    
    
    public function __construct()
    {
        $this->setDataCadastro(new DateTime());
        $this->setSaidas(new ArrayCollection());
    }

    
    public function getNome()
    {
        return $this->nome;
    }

    public function getUsuario()
    {
        return $this->usuario;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function setUsuario(Usuario $usuario)
    {
        $this->usuario = $usuario;
        return $this;
    }

    public function getLabel()
    {
        return $this->nome;
    }


}
