<?php

namespace NewWolf\EstoqueBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Usuario
 * @ORM\Table(name="usuario")
 * @ORM\Entity(repositoryClass="NewWolf\EstoqueBundle\Repository\UsuarioRepository")
 * @UniqueEntity(fields={"email", "cadastrado"},  message="Já existe um usuário com esse e-mail cadastrado", ignoreNull=false)
 */
class Usuario extends BaseEntity implements UserInterface
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=40, nullable=false)
     */
    private $token;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $senha;

    /**
     * @var Nivel
     *
     * @ORM\ManyToOne(targetEntity="Nivel", inversedBy="usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nivel", referencedColumnName="id")
     * })
     */
    private $nivel;
    
    /**
     * @var Cadastrado
     *
     * @ORM\ManyToOne(targetEntity="Cadastrado", inversedBy="usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cadastrado", referencedColumnName="id", nullable=false)
     * })
     */
    private $cadastrado;
    
    /**
     * @var int
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $ativo = TRUE;
    
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Produto", mappedBy="usuario")
     **/
    private $produtos;
    
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Categoria", mappedBy="usuario")
     **/
    private $categorias;
    
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Fornecedor", mappedBy="usuario")
     **/
    private $fornecedores;
    
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Entrada", mappedBy="usuario")
     **/
    private $entradas;
    
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Cliente", mappedBy="usuario")
     **/
    private $clientes;
    
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Saida", mappedBy="usuario")
     **/
    private $saidas;


    public function __construct(Cadastrado $cadastrado = null)
    {
        if ($cadastrado!=null) {
            $this->setCadastrado($cadastrado);
        }
        $this->setProdutos(new ArrayCollection());
        $this->setCategorias(new ArrayCollection());
        $this->setFornecedores(new ArrayCollection());
        $this->setEntradas(new ArrayCollection());
        $this->setClientes(new ArrayCollection());
        $this->setSaidas(new ArrayCollection());
        $this->setDataCadastro(new DateTime());
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return Usuario
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Usuario
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set senha
     *
     * @param string $senha
     * @return Usuario
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;

        return $this;
    }

    /**
     * Get senha
     *
     * @return string 
     */
    public function getSenha()
    {
        return $this->senha;
    }
    
    public function getNivel()
    {
        return $this->nivel;
    }

    public function getAtivo()
    {
        return $this->ativo;
    }

    public function setNivel(Nivel $nivel)
    {
        $this->nivel = $nivel;
        return $this;
    }

    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;
        return $this;
    }
    
    public function eraseCredentials()
    {
        
    }

    public function getPassword()
    {
        return $this->senha;
    }

    public function getRoles()
    {
        $roles = array();
        foreach ($this->getNivel()->getPermissoes() as $permissao) {
            $roles [] = "ROLE_".$permissao->getRole();
        }
        $roles [] = "ROLE_USER";
        return $roles;
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function getLabel()
    {
        return $this->getNome();
    }

    public function getCadastrado()
    {
        return $this->cadastrado;
    }

    public function setCadastrado(Cadastrado $cadastrado)
    {
        $this->cadastrado = $cadastrado;
        return $this;
    }

    public function getProdutos()
    {
        return $this->produtos;
    }

    public function setProdutos(Collection $produtos)
    {
        $this->produtos = $produtos;
        return $this;
    }


    public function getCategorias()
    {
        return $this->categorias;
    }

    public function getFornecedores()
    {
        return $this->fornecedores;
    }

    public function setCategorias(Collection $categorias)
    {
        $this->categorias = $categorias;
        return $this;
    }

    public function setFornecedores(Collection $fornecedores)
    {
        $this->fornecedores = $fornecedores;
        return $this;
    }

    public function getEntradas()
    {
        return $this->entradas;
    }

    public function setEntradas(Collection $entradas)
    {
        $this->entradas = $entradas;
        return $this;
    }

    public function getClientes()
    {
        return $this->clientes;
    }

    public function setClientes(Collection $clientes)
    {
        $this->clientes = $clientes;
        return $this;
    }


    public function getSaidas()
    {
        return $this->saidas;
    }

    public function setSaidas(Collection $saidas)
    {
        $this->saidas = $saidas;
        return $this;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }




}
