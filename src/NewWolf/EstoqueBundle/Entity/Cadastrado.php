<?php

namespace NewWolf\EstoqueBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * 
 * @ORM\Table(name="cadastrado")
 * @ORM\Entity
 * @UniqueEntity(fields="email", message="Esse e-mail já está cadastrado")
 * @author Luciano
 */
class Cadastrado extends BaseEntity
{
    
    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=150, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=150, nullable=false, unique=true)
     */
    private $email;
    
    /**
     * @var string 
     */
    private $senha;
        
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Usuario", mappedBy="cadastrado")
     **/
    private $usuarios;

    public function __construct()
    {
        $this->setUsuarios(new ArrayCollection());
        $this->setDataCadastro(new \DateTime());
    }


    public function getNome()
    {
        
        return $this->nome;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }
    
    public function getLabel()
    {
        return $this->getNome();
    }

    public function getSenha()
    {
        return $this->senha;
    }

    public function setSenha($senha)
    {
        $this->senha = $senha;
        return $this;
    }
    
    public function getUsuarios()
    {
        return $this->usuarios;
    }

    public function setUsuarios(Collection $usuarios)
    {
        $this->usuarios = $usuarios;
        return $this;
    }

}
