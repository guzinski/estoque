<?php

namespace NewWolf\EstoqueBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Permissao
 * @ORM\Table(name="permissao")
 * @ORM\Entity
 * @author Luciano
 */
class Permissao extends BaseEntity
{

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $nome;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $role;
    
    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Nivel", mappedBy="permissoes", cascade={"persist"})
     */
    private $niveis;
    
    public function __construct($nome = NULL, $role = NULL)
    {
        $this->setNome($nome)
                ->setRole($role)
                ->setDataCadastro(new \DateTime("now"))
                ->setNiveis(new ArrayCollection());
    }


    public function getNome()
    {
        return $this->nome;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function getNiveis()
    {
        return $this->niveis;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    public function setNiveis(Collection $niveis)
    {
        $this->niveis = $niveis;
        return $this;
    }

        
    public function getLabel()
    {
        return $this->getNome();
    }
    

}
