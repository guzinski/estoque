<?php

namespace NewWolf\EstoqueBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Nivel
 * 
 * @ORM\Table(name="nivel")
 * @ORM\Entity
 * @author Luciano
 */
class Nivel extends BaseEntity
{
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $nome;
    
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Usuario", mappedBy="nivel")
     **/
    private $usuarios;
    
    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Permissao", inversedBy="niveis", cascade={"persist"})
     * @ORM\JoinTable(name="nivel_permissao",
     *   joinColumns={
     *     @ORM\JoinColumn(name="nivel", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="permissao", referencedColumnName="id")
     *   }
     * )
     */
    private $permissoes;


    public function __construct($nome = NULL)
    {
        $this->setNome($nome)
                ->setDataCadastro(new DateTime("now"))
                ->setUsuarios(new ArrayCollection())
                ->setPermissoes(new ArrayCollection());
    }
    

    public function getNome()
    {
        return $this->nome;
    }

    public function getUsuarios()
    {
        return $this->usuarios;
    }

    public function getPermissoes()
    {
        return $this->permissoes;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function setUsuarios(Collection $usuarios)
    {
        $this->usuarios = $usuarios;
        return $this;
    }

    public function setPermissoes(Collection $permissoes)
    {
        $this->permissoes = $permissoes;
        return $this;
    }

    /**
     * 
     * 
     * @return type
     */
    public function getLabel()
    {
        return $this->getNome();
    }

}
