<?php

namespace NewWolf\EstoqueBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Description of Saida
 * @ORM\Table(name="saida")
 * @ORM\Entity(repositoryClass="NewWolf\EstoqueBundle\Repository\SaidaRepository")
 */
class Saida extends BaseEntity
{
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="data_saida", type="datetime", nullable=false)
     */
    private $dataSaida;
    
    /**
     * @var float
     *
     * @ORM\Column(name="valor_venda", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valorVenda;
    
    /**
     * @var int
     *
     * @ORM\Column(name="quantidade", type="integer", nullable=false)
     */
    private $quantidade;

    /**
     * @var Produto
     *
     * @ORM\ManyToOne(targetEntity="Produto", inversedBy="saidas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="produto", referencedColumnName="id")
     * })
     */
    private $produto;

    
    /**
     * @var float
     *
     * @ORM\Column(name="desconto", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $desconto;    
        
    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="saidas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario", referencedColumnName="id")
     * })
     */
    private $usuario;
    
    /**
     * @var Cliente
     *
     * @ORM\ManyToOne(targetEntity="Cliente", inversedBy="saidas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cliente", referencedColumnName="id")
     * })
     */
    private $cliente;

    public function __construct()
    {
        $this->setDataCadastro(new DateTime("now"))
            ->setDataSaida(new DateTime("now"));
    }
    
    public function getDataSaida()
    {
        return $this->dataSaida;
    }

    public function getValorVenda()
    {
        return $this->valorVenda;
    }

    public function getQuantidade()
    {
        return $this->quantidade;
    }

    public function getProduto()
    {
        return $this->produto;
    }

    public function getDesconto()
    {
        return $this->desconto;
    }

    public function getUsuario()
    {
        return $this->usuario;
    }

    public function setDataSaida(DateTime $dataSaida)
    {
        $this->dataSaida = $dataSaida;
        return $this;
    }

    public function setValorVenda($valorVenda)
    {
        $this->valorVenda = $valorVenda;
        return $this;
    }

    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
        return $this;
    }

    public function setProduto(Produto $produto)
    {
        $this->produto = $produto;
        return $this;
    }

    public function setDesconto($desconto)
    {
        $this->desconto = $desconto;
        return $this;
    }

    public function setUsuario(Usuario $usuario)
    {
        $this->usuario = $usuario;
        return $this;
    }

    public function getLabel()
    {
        $this->getProduto()->getLabel();
    }
    
    public function getCliente()
    {
        return $this->cliente;
    }

    public function setCliente(Cliente $cliente = null)
    {
        $this->cliente = $cliente;
        return $this;
    }



}
