<?php
namespace NewWolf\EstoqueBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Produto
 * @ORM\Table(name="produto")
 * @ORM\Entity(repositoryClass="NewWolf\EstoqueBundle\Repository\ProdutoRepository")
 * @author Luciano
 */
class Produto extends BaseEntity
{
            
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $nome;
    
    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $descricao;
    
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private $quantidadeMinima;
    
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private $quantidadeInicial;
    
    /**
     * @var float
     *
     * @ORM\Column(name="preco_medio", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $precoMedio;
    
    /**
     * @var float
     *
     * @ORM\Column(name="preco_venda", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $precoVenda;

    
    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="produtos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var Categoria
     *
     * @ORM\ManyToOne(targetEntity="Categoria", inversedBy="produtos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoria", referencedColumnName="id")
     * })
     */
    private $categoria;
    
    /**
     * @var Fornecedor
     *
     * @ORM\ManyToOne(targetEntity="Fornecedor", inversedBy="produtos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fornecedor", referencedColumnName="id")
     * })
     */
    private $fornecedor;
    
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Entrada", mappedBy="produto")
     **/
    private $entradas;
    
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Saida", mappedBy="produto")
     **/
    private $saidas;

    
    public function __construct()
    {
        $this->setSaidas(new ArrayCollection());
        $this->setEntradas(new ArrayCollection());
        $this->setDataCadastro(new DateTime());
    }
    
    public function getCodigo()
    {
        return $this->codigo;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function getQuantidadeMinima()
    {
        return $this->quantidadeMinima;
    }

    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
        return $this;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    public function setQuantidadeMinima($quantidadeMinima)
    {
        $this->quantidadeMinima = $quantidadeMinima;
        return $this;
    }
    
    /**
     * 
     * @return type
     */
    public function getLabel()
    {
        return $this->getNome();
    }

    public function getUsuario()
    {
        return $this->usuario;
    }

    public function setUsuario(Usuario $usuario)
    {
        $this->usuario = $usuario;
        return $this;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }

    public function setCategoria(Categoria $categoria)
    {
        $this->categoria = $categoria;
        return $this;
    }

    public function getEntradas()
    {
        return $this->entradas;
    }

    public function setEntradas(Collection $entradas)
    {
        $this->entradas = $entradas;
        return $this;
    }

    public function getFornecedor()
    {
        return $this->fornecedor;
    }

    public function setFornecedor(Fornecedor $fornecedor)
    {
        $this->fornecedor = $fornecedor;
        return $this;
    }

    public function getPrecoMedio()
    {
        return $this->precoMedio;
    }

    public function getPrecoVenda()
    {
        return $this->precoVenda;
    }

    public function setPrecoMedio($precoMedio)
    {
        $this->precoMedio = $precoMedio;
        return $this;
    }

    public function setPrecoVenda($precoVenda)
    {
        $this->precoVenda = $precoVenda;
        return $this;
    }

    public function getSaidas()
    {
        return $this->saidas;
    }

    public function setSaidas(Collection $saidas)
    {
        $this->saidas = $saidas;
        return $this;
    }


    public function getQuantidadeInicial()
    {
        return $this->quantidadeInicial;
    }

    public function setQuantidadeInicial($quantidadeInicial)
    {
        $this->quantidadeInicial = $quantidadeInicial;
        return $this;
    }



    
}
