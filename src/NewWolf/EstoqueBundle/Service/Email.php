<?php

namespace NewWolf\EstoqueBundle\Service;

use Swift_Mailer;
use Swift_Message;

/**
 * Description of Email
 *
 * @author Luciano
 */
class Email
{
    
    

    /**
     *
     * @var Swift_Message
     */
    private $mail;
    
    
    protected $mailer;

    public function setMailer(Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }


    public function __construct(Swift_Mailer $mailer)
    {
        $this->setMailer($mailer);
        $this->setMail(Swift_Message::newInstance());
        $this->getMail()->setFrom('estoque@newwolf.com.br', "Sistema de Estoque");
    }

    /**
     *
     * @return Swift_Message
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     *
     * @param Swift_Message $mail
     */
    public function setMail(Swift_Message $mail)
    {
        $this->mail = $mail;
    }

    /**
     *
     * @param string $email
     * @param string $senha
     * @param string $controller
     */
    public function convidarUsuario($nome, $email, $url)
    {
        $this->getMail()
                ->setSubject('Você foi convidado para o sistema de estoque')
                ->setTo($email, $nome)
                ->setBody("Você foi convidado poara  acessar  o sistema de estqoue. Clique no link abaixo para compeltar seu cadstro: {$url}");

        $this->mailer->send($this->getMail());
    }

    /**
     *
     * @param string $email
     * @param string $senha
     * @param string $controller
     */
    public function enviaCadastroEmpresa($email, $senha, $controller)
    {
        $this->getMail()
                ->setSubject('Cadastro concluído')
                ->setTo($email)
                ->setBody(
                    $controller->renderView(
                        'CGESiteBundle::Email\cadastroEmpresa.html.twig',
                        array(
                                'email' => $email,
                                'senha' => $senha,
                        )
                    ),
                    "text/html"
                );

        $controller->get('mailer')->send($this->getMail());
    }

    /**
     *
     * @param string $email
     * @param string $senha
     * @param string $controller
     */
    public function enviaCadastroEmpresa2($razaoSocial, $controller)
    {
        $this->getMail()
                ->setSubject('Nova Empresa Cadastrada - '.$razaoSocial)
                ->setTo("adriano@cge-rs.com.br", "Adriano")
                ->addCc("marianna@cge-rs.com.br", "Marianna")
                ->addCc("comercial@cge-rs.com.br")
                ->setBody(
                    $controller->renderView(
                        'CGESiteBundle::Email\cadastroEmpresa2.html.twig',
                        array(
                                'razaoSocial' => $razaoSocial,
                        )
                    ),
                    "text/html"
                );

        $controller->get('mailer')->send($this->getMail());
    }
}
