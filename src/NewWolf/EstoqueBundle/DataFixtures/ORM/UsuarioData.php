<?php

namespace NewWolf\EstoqueBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NewWolf\EstoqueBundle\Entity\Cadastrado;
use NewWolf\EstoqueBundle\Entity\Nivel;
use NewWolf\EstoqueBundle\Entity\Permissao;
use NewWolf\EstoqueBundle\Entity\Usuario;

/**
 * Description of NivelData
 *
 * @author Luciano
 */
class UsuarioData implements FixtureInterface
{
    
    
    public function load(ObjectManager $manager)
    {
        $cadastrado = new Cadastrado();
        $cadastrado->setEmail("lucianoguzinski@gmail.com")
                ->setNome("Luciano Guzinski")
                ->setSenha("123456");
        
        $nivel = new Nivel("Administrador");
                
        $nivel->getPermissoes()->add(new Permissao("Usuários", "USUARIO"));
        $nivel->getPermissoes()->add(new Permissao("Nível", "NIVEL"));
        $nivel->getPermissoes()->add(new Permissao("Produto", "PRODUTO"));
        $nivel->getPermissoes()->add(new Permissao("Entrada", "ENTRADA"));
        $nivel->getPermissoes()->add(new Permissao("Saída", "SAIDA"));

        $manager->persist($nivel);
        
        $usuario = new Usuario();
        $usuario->setAtivo(true)
                ->setEmail("lucianoguzinski@gmail.com")
                ->setNome("Luciano Guzinski")
                ->setSenha(hash("sha512", "123456"))
                ->setNivel($nivel)
                ->setToken(sha1(time()))
                ->setCadastrado($cadastrado);
        
        $manager->persist($cadastrado);
        $manager->persist($usuario);
        $manager->flush();
    }

    
    
    
    
    
}
