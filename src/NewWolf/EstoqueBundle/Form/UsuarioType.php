<?php

namespace NewWolf\EstoqueBundle\Form;

use NewWolf\EstoqueBundle\Entity\Nivel;
use NewWolf\EstoqueBundle\Entity\Usuario;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of UsuarioType
 *
 * @author Luciano
 */
class UsuarioType extends AbstractType
{
    
    public function getBlockPrefix()
    {
        return "usuario";
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nome')
                ->add('email', EmailType::class, ['label'=>'E-mail', 'attr'=>['help'=>"Um convite será enviado para esse e-mail"]])
                ->add('nivel', EntityType::class, array(
                    'class' => Nivel::class,
                    'placeholder' => 'Selecione',
                    'empty_data' => null,
                    'label' => 'Nível'
                ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Usuario::class
        ));
    }

}
