<?php

namespace NewWolf\EstoqueBundle\Form;

use Doctrine\ORM\EntityRepository;
use NewWolf\EstoqueBundle\Entity\Entrada;
use NewWolf\EstoqueBundle\Entity\Produto;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Description of EntradaType
 *
 * @author Luciano
 */
class EntradaType extends AbstractType
{
    
    /**
     *
     * @var TokenStorage 
     */
    private $securityContext;
        
    public function __construct(TokenStorage $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    public function getBlockPrefix()
    {
        return "entrada";
    }
    
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $cadastrado = $this->securityContext->getToken()->getUser()->getCadastrado();
        $builder
            ->add("data", DateType::class, array(
                        'label'  => 'Data da Compra',
                        'widget' => 'single_text',
                        'format' => 'dd/MM/yyyy',
              ))
            ->add('produto', EntityType::class, array(
                    'class'         => Produto::class,
                    'placeholder'   => 'Selecione',
                    'empty_data'    => null,
                    'query_builder' => function(EntityRepository $er) use ($cadastrado) {
                        return $er->getQueryByCadastrado($cadastrado);
                    },
            ))
            ->add("valorUnitario", MoneyType::class, array("currency"=>"", "grouping"=>false))
            ->add('quantidade')
                            
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Entrada::class
        ));
    }


}
