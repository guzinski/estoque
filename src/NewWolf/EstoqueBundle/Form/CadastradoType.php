<?php

namespace NewWolf\EstoqueBundle\Form;

use NewWolf\EstoqueBundle\Entity\Cadastrado;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Description of Cadastrado
 *
 * @author Luciano
 */
class CadastradoType extends AbstractType
{
    
    public function getBlockPrefix()
    {
        return "cadastro";
    }
    
    /**
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("email", EmailType::class, [
                    'constraints' => [
                        new NotBlank(),
                        new Email(),
                    ]
                ])
                ->add("senha", PasswordType::class, [
                    'constraints' => [
                        new NotBlank()
                    ]
                ])              
                ->add("nome", TextType::class, ["label" => "Nome Completo",
                    'constraints' => [
                        new NotBlank()
                    ]
                ]);
    }
    
    
        /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Cadastrado::class
        ));
    }


}
