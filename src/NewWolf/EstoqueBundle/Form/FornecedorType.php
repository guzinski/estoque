<?php

namespace NewWolf\EstoqueBundle\Form;

use NewWolf\EstoqueBundle\Entity\Fornecedor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of FornecedorType
 *
 * @author Luciano
 */
class FornecedorType extends AbstractType
{
    public function getBlockPrefix()
    {
        return "fornecedor";
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nome');
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Fornecedor::class
        ));
    }
}
