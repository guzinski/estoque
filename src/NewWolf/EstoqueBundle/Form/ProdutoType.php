<?php

namespace NewWolf\EstoqueBundle\Form;

use Doctrine\ORM\EntityRepository;
use NewWolf\EstoqueBundle\Entity\Fornecedor;
use NewWolf\EstoqueBundle\Entity\Produto;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ProdutoType extends AbstractType
{
    /**
     *
     * @var TokenStorage 
     */
    private $securityContext;
        
    public function __construct(TokenStorage $securityContext)
    {
        $this->securityContext = $securityContext;
    }
    
    public function getBlockPrefix()
    {
        return "produto";
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $cadastrado = $this->securityContext->getToken()->getUser()->getCadastrado();
        $builder
            ->add('codigo')
            ->add('categoria', EntityType::class, array(
                    'class'         => \NewWolf\EstoqueBundle\Entity\Categoria::class,
                    'placeholder'   => 'Selecione',
                    'empty_data'    => null,
                    'query_builder' => function(EntityRepository $er) use ($cadastrado) {
                        return $er->getQueryByCadastrado($cadastrado);
                    },

            ))
            ->add('fornecedor', EntityType::class, array(
                    'class'         => Fornecedor::class,
                    'placeholder'   => 'Selecione',
                    'empty_data'    => null,
                    'query_builder' => function(EntityRepository $er) use ($cadastrado) {
                        return $er->getQueryByCadastrado($cadastrado);
                    },
            ))

            ->add('nome')
            ->add('descricao')
            ->add('quantidadeMinima')
            ->add('quantidadeInicial')
            ->add("precoVenda", MoneyType::class, array("currency"=>"", "grouping"=>false))
            ->add("precoMedio", MoneyType::class, array("currency"=>"", "grouping"=>false))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Produto::class
        ));
    }
}
