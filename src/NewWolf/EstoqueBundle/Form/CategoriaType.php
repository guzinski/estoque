<?php

namespace NewWolf\EstoqueBundle\Form;

use NewWolf\EstoqueBundle\Entity\Categoria;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Description of CategoriaType
 *
 * @author Luciano
 */
class CategoriaType extends AbstractType
{
    public function getBlockPrefix()
    {
        return "categoria";
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nome');
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Categoria::class
        ));
    }
}
