<?php

namespace NewWolf\EstoqueBundle\Form;

use Doctrine\ORM\EntityRepository;
use NewWolf\EstoqueBundle\Entity\Cliente;
use NewWolf\EstoqueBundle\Entity\Produto;
use NewWolf\EstoqueBundle\Entity\Saida;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Description of SaidaType
 *
 * @author Luciano
 */
class SaidaType extends AbstractType
{
    /**
     * Constante com o nome do Form
     */
    const SAIDA = "saida";
    
    /**
     *
     * @var TokenStorage 
     */
    private $securityContext;
        
    public function __construct(TokenStorage $securityContext)
    {
        $this->securityContext = $securityContext;
    }
    
    

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $cadastrado = $this->securityContext->getToken()->getUser()->getCadastrado();
        $builder
            ->add("dataSaida", DateType::class, array(
                        'label'  => 'Data da Venda',
                        'widget' => 'single_text',
                        'format' => 'dd/MM/yyyy',
              ))
            ->add('produto', EntityType::class, array(
                    'class'         => Produto::class,
                    'placeholder'   => 'Selecione',
                    'empty_data'    => null,
                    'query_builder' => function(EntityRepository $er) use ($cadastrado) {
                        return $er->getQueryByCadastrado($cadastrado);
                    },
            ))
            ->add('cliente', EntityType::class, array(
                    'class'         => Cliente::class,
                    'placeholder'   => 'Selecione',
                    'empty_data'    => null,
                    'query_builder' => function(EntityRepository $er) use ($cadastrado) {
                        return $er->getQueryByCadastrado($cadastrado);
                    },
                    'required' => false,
            ))
            ->add("valorVenda", MoneyType::class, array("currency"=>"", "grouping"=>false))
            ->add('quantidade')
            ->add('desconto')
                            
        ;
    }

    public function getBlockPrefix()
    {
        return self::SAIDA;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Saida::class
        ]);
    }

}
