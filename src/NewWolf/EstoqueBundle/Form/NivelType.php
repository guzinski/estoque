<?php

namespace NewWolf\EstoqueBundle\Form;

use NewWolf\EstoqueBundle\Entity\Nivel;
use NewWolf\EstoqueBundle\Entity\Permissao;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of NivelType
 *
 * @author Luciano
 */
class NivelType extends AbstractType
{

    public function getBlockPrefix()
    {
        return "nivel";
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('permissoes', EntityType::class, array(
            'class' => Permissao::class,
            'label' => 'Permissões',
            'multiple' => true,
            'expanded' => true,
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Nivel::class
        ));
    }

}
