<?php

namespace NewWolf\EstoqueBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use NewWolf\EstoqueBundle\Entity\Cadastrado;

/**
 * Description of UsuarioRepository
 *
 * @author Luciano
 */
class UsuarioRepository extends BaseRepository
{
    /**
     * 
     * @param Cadastrado $cadastrado
     * @return QueryBuilder
     */
    public function getQueryByCadastrado(Cadastrado $cadastrado) 
    {
        $query = $this->createQueryBuilder("U")
            ->where('U.cadastrado = :cadastrado')                
            ->setParameter('cadastrado', $cadastrado);
        
        return $query;
    }
}
