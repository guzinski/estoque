<?php

namespace NewWolf\EstoqueBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use NewWolf\EstoqueBundle\Entity\Cadastrado;


/**
 * Description of BaseRepository
 *
 * @author Luciano
 */
abstract class BaseRepository extends EntityRepository
{
    
    /**
     * 
     * @param Cadastrado $cadastrado
     * @return QueryBuilder
     */
    public function getQueryByCadastrado(Cadastrado $cadastrado) 
    {
        $query = $this->createQueryBuilder("E")
            ->leftJoin("E.usuario", "U")
            ->where('U.cadastrado = :cadastrado')                
            ->setParameter('cadastrado', $cadastrado);
        
        return $query;
    }
    
    /**
     * 
     * @param Cadastrado $cadastrado
     * @return array
     */
    public function getByCadastrado(Cadastrado $cadastrado) 
    {
        return $this->getQueryByCadastrado($cadastrado)->getQuery()->getResult();
    }
    
}
