<?php

namespace NewWolf\EstoqueBundle\Controller;

use NewWolf\EstoqueBundle\Entity\Cliente;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of CLienteController
 * @Route("/cliente")
 */
class ClienteController extends Controller
{

    /**
     * @Route("/", name="cliente_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
    
    /**
     * 
     * @Route("/pagination", name="cliente_pagination")
     * @Method("GET")
     */
    public function paginationAction()
    {
        $clientes = $this->getDoctrine()->getRepository("NewWolfEstoqueBundle:CLiente")->getByCadastrado($this->getUser()->getCadastrado());
        $dados = [];
        foreach ($clientes as $cliente) {
            $dados[] = [
                "<input type='checkbox' />",
                "<a href=\"".$this->generateUrl("cliente_edit", array("id"=>$cliente->getid())) ."\">". $cliente->getNome(). "</a>",
            ];
        }
        return new Response(json_encode([
            'recordsTotal' => count($clientes),
            'recordsFiltered' => count($clientes),
            'data' => $dados,
        ]));
    }

    /**
     * Creates a new Categoria entity.
     *
     * @Route("/new", name="cliente_new")
     * @Method({"GET", "POST"})
     * @Template("NewWolfEstoqueBundle::Cliente/form.html.twig")
     */
    public function newAction(Request $request)
    {
        return $this->salvarCategoria($request, new Cliente());
    }

    /**
     *
     * @Route("/{id}/edit", name="cliente_edit")
     * @Method({"GET", "POST"})
     * @Template("NewWolfEstoqueBundle::Cliente/form.html.twig")
     */
    public function editAction(Request $request, Cliente $cliente)
    {
        return $this->salvarCategoria($request, $cliente);
    }

    
    private function salvarCategoria(Request $request, Cliente $cliente)
    {
        $form = $this->createForm('NewWolf\EstoqueBundle\Form\ClienteType', $cliente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cliente->setUsuario($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($cliente);
            $em->flush();

            return $this->redirectToRoute('cliente_index', array('id' => $cliente->getId()));
        }

        return array(
            'cliente' => $cliente,
            'form' => $form->createView(),
        );
    }
}
