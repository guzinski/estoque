<?php

namespace NewWolf\EstoqueBundle\Controller;

use NewWolf\EstoqueBundle\Entity\Produto;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Produto controller.
 *
 * @Route("/produto")
 */
class ProdutoController extends Controller
{
    /**
     * Lists all Produto entities.
     *
     * @Route("/", name="produto_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
    
    /**
     * 
     * @Route("/pagination", name="produto_pagination")
     * @Method("GET")
     */
    public function paginationAction()
    {
        $produtos = $this->getDoctrine()->getRepository(Produto::class)->getByCadastrado($this->getUser()->getCadastrado());
        $dados = [];
        foreach ($produtos as $produto) {
            $linha = [
                "<input type='checkbox' />",
                "<a href=\"".$this->generateUrl("produto_edit", array("id"=>$produto->getid())) ."\">". $produto->getNome(). " - ". $produto->getCodigo() ."</a>",
            ];
            $dados[] = $linha;
        }
        return new Response(json_encode([
            'recordsTotal' => count($produtos),
            'recordsFiltered' => count($produtos),
            'data' => $dados,
        ]));
    }


    /**
     * Creates a new Produto entity.
     *
     * @Route("/new", name="produto_new")
     * @Method({"GET", "POST"})
     * @Template("NewWolfEstoqueBundle::Produto/form.html.twig")
     */
    public function newAction(Request $request)
    {
        return $this->salvarProduto($request, new Produto());
    }

    /**
     *
     * @Route("/{id}/edit", name="produto_edit")
     * @Method({"GET", "POST"})
     * @Template("NewWolfEstoqueBundle::Produto/form.html.twig")
     */
    public function editAction(Request $request, Produto $produto)
    {
        return $this->salvarProduto($request, $produto);
    }    

    /**
     * Deletes a Produto entity.
     *
     * @Route("/{id}", name="produto_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Produto $produto)
    {
//        $form = $this->createDeleteForm($produto);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->remove($produto);
//            $em->flush();
//        }
//
//        return $this->redirectToRoute('produto_index');
    }
    
    private function salvarProduto(Request $request, Produto $produto)
    {
        $form = $this->createForm($this->get("produto.type"), $produto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $produto->setUsuario($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($produto);
            $em->flush();

            return $this->redirectToRoute('produto_index', array('id' => $produto->getId()));
        }

        return array(
            'produto' => $produto,
            'form' => $form->createView(),
        );
    }

    
    
}
