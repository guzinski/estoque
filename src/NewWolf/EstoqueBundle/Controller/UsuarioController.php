<?php

namespace NewWolf\EstoqueBundle\Controller;

use NewWolf\EstoqueBundle\Entity\Usuario;
use NewWolf\EstoqueBundle\Form\UsuarioType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * UsuarioController
 *
 * @author Luciano
 */
class UsuarioController extends Controller
{    
    /**
     * @Route("/usuario", name="usuario_index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/usuario/pagination", name="usuario_pagination")
     * @return Response
     */
    public function paginationAction()
    {
        $usuarios = $this->getDoctrine()->getRepository(Usuario::class)->getByCadastrado($this->getUser()->getCadastrado());
        $dados = [];
        foreach ($usuarios as $usuario) {
            $linha = array();
            
            $dados[] = [
                $this->renderView("NewWolfEstoqueBundle::Form/checkbox.html.twig", ['id'=>$usuario->getId()]),
                "<a href=\"".$this->generateUrl("usuario_form", array("id"=>$usuario->getId())) ."\">". $usuario->getNome() ."</a>",
                $usuario->getEmail(),
                $usuario->getAtivo() ? "Ativo" : "Inativo",
            ];
        }
        $return['recordsTotal'] = count($usuarios);
        $return['recordsFiltered'] = count($usuarios);
        $return['data'] = $dados;
        return new Response(json_encode($return));
    }

    
    /**
     * 
     * @Route("/usuario/editar/{id}", name="usuario_form")
     * @Template()
     */
    public function formAction(Request $request, $id = 0) 
    {
        $em = $this->getDoctrine()->getManager();
        
        if ($id>0) {
            $usuario = $em->find(Usuario::class, $id);
        } else {
            $usuario = new Usuario($this->getUser()->getCadastrado());
        }
        
        $form = $this->createForm(new UsuarioType(), $usuario);
        
        $form->handleRequest($request);
        if ($form->isValid()) {
            $usuario->setToken(sha1(time()));
            $em->persist($usuario);
            $em->flush();
            
            $this->get('my_mailer')->convidarUsuario($usuario->getNome(), $usuario->getEmail(), $this->generateUrl("usuario_convite", ['token'=>  $usuario->getToken()], UrlGeneratorInterface::ABSOLUTE_URL));
            
            return $this->redirectToRoute("usuario_index");
        }
        
        return array("usuario"=>$usuario, "form"=>$form->createView());
    }

    
    /**
     * @Route("/convite/{token}", name="usuario_convite")
     * @Template()
     */
    public function conviteAction($token)
    {
        return array();
    }
    
    /**
     * @Route("/usuario/excluir", name="usuario_excluir")
     */
    public function excluiUsuarioAction(Request $resquest) 
    {
        $respone = array();
        $id = $resquest->request->getInt("id", null);
        if (null != $id) {
            $em = $this->getDoctrine()->getManager();
            $usuario = $em->find(Usuario::class, $id);
            $em->remove($usuario);
            $em->flush();
            $respone['ok'] = 1;
        } else {
            $respone['ok'] = 0;
            $respone['error'] = "Erro ao exclui usuário";
        }
        return new Response(json_encode($respone));
    }

    
}
