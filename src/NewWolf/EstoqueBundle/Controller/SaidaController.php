<?php

namespace NewWolf\EstoqueBundle\Controller;

use NewWolf\EstoqueBundle\Entity\Saida;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of SaidaController
 * @Route("/saida")
 * @author Luciano
 */
class SaidaController extends Controller
{
    /**
     * @Route("/", name="saida_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
    
    /**
     * 
     * @Route("/pagination", name="saida_pagination")
     * @Method("GET")
     */
    public function paginationAction()
    {
        $saidas = $this->getDoctrine()->getRepository("NewWolfEstoqueBundle:Saida")->getByCadastrado($this->getUser()->getCadastrado());
        $dados = [];
        foreach ($saidas as $saida) {
            $dados[] = [
                "<input type='checkbox' />",
                "<a href=\"".$this->generateUrl("saida_edit", array("id"=>$saida->getid())) ."\">". $saida->getProduto()->getLabel()." ".$saida->getDataSaida()->format("d/m/Y") . "</a>",
                $saida->getQuantidade(),
                "R$ ".  number_format($saida->getValorVenda(), 2, ",", "."),
            ];
        }
        return new Response(json_encode([
            'recordsTotal' => count($saidas),
            'recordsFiltered' => count($saidas),
            'data' => $dados,
        ]));
    }
    
    
    /**
     * Creates a new Saida entity.
     *
     * @Route("/new", name="saida_new")
     * @Method({"GET", "POST"})
     * @Template("NewWolfEstoqueBundle::Saida/form.html.twig")
     */
    public function newAction(Request $request)
    {
        return $this->salvarSaida($request, new Saida());
    }

    /**
     *
     * @Route("/{id}/edit", name="saida_edit")
     * @Method({"GET", "POST"})
     * @Template("NewWolfEstoqueBundle::Saida/form.html.twig")
     */
    public function editAction(Request $request, Saida $saida)
    {
        return $this->salvarSaida($request, $saida);
    }

    
    private function salvarSaida(Request $request, Saida $saida)
    {
        $form = $this->createForm($this->get("saida.type"), $saida);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $saida->setUsuario($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($saida);
            $em->flush();

            return $this->redirectToRoute('saida_index', array('id' => $saida->getId()));
        }

        return array(
            'saida' => $saida,
            'form' => $form->createView(),
        );
    }


    
    
}
