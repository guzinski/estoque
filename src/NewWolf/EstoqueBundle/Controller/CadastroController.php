<?php

namespace NewWolf\EstoqueBundle\Controller;

use NewWolf\EstoqueBundle\Entity\Cadastrado;
use NewWolf\EstoqueBundle\Entity\Usuario;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of CadastroController
 *
 * @author Luciano
 * @Route("/cadastro")
 */
class CadastroController extends Controller
{
    
    /**
     * 
     * @param type $param
     * @Route("", name="cadastro_index")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $cadastrado = new Cadastrado();
        $form = $this->createForm('NewWolf\EstoqueBundle\Form\CadastradoType', $cadastrado);
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $usuario = new Usuario();
            $usuario->setAtivo(1);
            $usuario->setSenha(hash("sha512", $cadastrado->getSenha()));
            $usuario->setNome($cadastrado->getNome());
            $usuario->setEmail($cadastrado->getEmail());
            $usuario->setCadastrado($cadastrado);
            
            $em = $this->getDoctrine()->getManager();
            $usuario->setNivel($em->getRepository("NewWolfEstoqueBundle:Nivel")->findOneBy(['nome'=>"Administrador"]));
            $em->persist($cadastrado);
            $em->persist($usuario);
            $em->flush();
            return $this->redirectToRoute('_login');
        }
        return array(
            'form' => $form->createView(),
        );
    }
    
    
}
