<?php

namespace NewWolf\EstoqueBundle\Controller;

use NewWolf\EstoqueBundle\Entity\Entrada;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of EntradaController
 * @Route("/entrada")
 * @author Luciano
 */
class EntradaController extends Controller
{
    /**
     * @Route("/", name="entrada_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
    
    /**
     * 
     * @Route("/pagination", name="entrada_pagination")
     * @Method("GET")
     */
    public function paginationAction()
    {
        $entradas = $this->getDoctrine()->getRepository(Entrada::class)->getByCadastrado($this->getUser()->getCadastrado());
        $dados = [];
        foreach ($entradas as $entrada) {
            $dados[] = [
                "<input type='checkbox' />",
                "<a href=\"".$this->generateUrl("entrada_edit", array("id"=>$entrada->getid())) ."\">". $entrada->getProduto()->getLabel()." ".$entrada->getData()->format("d/m/Y") . "</a>",
                $entrada->getQuantidade(),
                "R$ ".  number_format($entrada->getValorUnitario(), 2, ",", "."),
            ];
        }
        return new Response(json_encode([
            'recordsTotal' => count($entradas),
            'recordsFiltered' => count($entradas),
            'data' => $dados,
        ]));
    }
    
    /**
     * Creates a new Compra entity.
     *
     * @Route("/new", name="entrada_new")
     * @Method({"GET", "POST"})
     * @Template("NewWolfEstoqueBundle::Entrada/form.html.twig")
     */
    public function newAction(Request $request)
    {
        return $this->salvarEntrada($request, new Entrada());
    }

    /**
     *
     * @Route("/{id}/edit", name="entrada_edit")
     * @Method({"GET", "POST"})
     * @Template("NewWolfEstoqueBundle::Entrada/form.html.twig")
     */
    public function editAction(Request $request, Entrada $entrada)
    {
        return $this->salvarEntrada($request, $entrada);
    }
    
    private function salvarEntrada(Request $request, Entrada $entrada)
    {
        $form = $this->createForm($this->get("entrada.type"), $entrada);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entrada->setUsuario($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($entrada);
            $em->flush();

            return $this->redirectToRoute('entrada_index', array('id' => $entrada->getId()));
        }

        return array(
            'entrada' => $entrada,
            'form' => $form->createView(),
        );
    }
}
