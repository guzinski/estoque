<?php

namespace NewWolf\EstoqueBundle\Controller;

use NewWolf\EstoqueBundle\Entity\Categoria;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of CategoriaController
 * @Route("/categoria")
 * @author Luciano
 */
class CategoriaController extends Controller
{
    
    /**
     * @Route("/", name="categoria_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
    
    
    /**
     * 
     * @Route("/pagination", name="categoria_pagination")
     * @Method("GET")
     */
    public function paginationAction()
    {
        $categorias = $this->getDoctrine()->getRepository("NewWolfEstoqueBundle:Categoria")->getByCadastrado($this->getUser()->getCadastrado());
        $dados = [];
        foreach ($categorias as $categoria) {
            $dados[] = [
                $this->renderView("NewWolfEstoqueBundle::Form/checkbox.html.twig", ['id'=>$categoria->getId()]),
                "<a href=\"".$this->generateUrl("categoria_edit", array("id"=>$categoria->getid())) ."\">". $categoria->getNome(). "</a>",
            ];
        }
        return new Response(json_encode([
            'recordsTotal' => count($categorias),
            'recordsFiltered' => count($categorias),
            'data' => $dados,
        ]));
    }

    /**
     * Creates a new Categoria entity.
     *
     * @Route("/new", name="categoria_new")
     * @Method({"GET", "POST"})
     * @Template("NewWolfEstoqueBundle::Categoria/form.html.twig")
     */
    public function newAction(Request $request)
    {
        return $this->salvarCategoria($request, new Categoria());
    }

    /**
     *
     * @Route("/{id}/edit", name="categoria_edit")
     * @Method({"GET", "POST"})
     * @Template("NewWolfEstoqueBundle::Categoria/form.html.twig")
     */
    public function editAction(Request $request, Categoria $categoria)
    {
        return $this->salvarCategoria($request, $categoria);
    }

    
    private function salvarCategoria(Request $request, Categoria $categoria)
    {
        $form = $this->createForm('NewWolf\EstoqueBundle\Form\CategoriaType', $categoria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoria->setUsuario($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($categoria);
            $em->flush();

            return $this->redirectToRoute('categoria_index', array('id' => $categoria->getId()));
        }

        return array(
            'categoria' => $categoria,
            'form' => $form->createView(),
        );
    }

    
}
