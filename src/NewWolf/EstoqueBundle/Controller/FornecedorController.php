<?php

namespace NewWolf\EstoqueBundle\Controller;

use NewWolf\EstoqueBundle\Entity\Fornecedor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of FornecedorController
 *
 * @Route("/fornecedor")
 * @author Luciano
 */
class FornecedorController extends Controller
{
    
    /**
     * Lists all Fornecedor entities.
     *
     * @Route("/", name="fornecedor_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
    
    /**
     * 
     * @Route("/pagination", name="fornecedor_pagination")
     * @Method("GET")
     */
    public function paginationAction()
    {
        $fornecedores = $this->getDoctrine()->getRepository("NewWolfEstoqueBundle:Fornecedor")->getByCadastrado($this->getUser()->getCadastrado());
        $dados = [];
        foreach ($fornecedores as $fornecedor) {
            $dados[] = [
                "<input type='checkbox' />",
                "<a href=\"".$this->generateUrl("fornecedor_edit", array("id"=>$fornecedor->getid())) ."\">". $fornecedor->getNome(). "</a>",
            ];
        }
        return new Response(json_encode([
            'recordsTotal' => count($fornecedores),
            'recordsFiltered' => count($fornecedores),
            'data' => $dados,
        ]));
    }
    
    /**
     * Creates a new Fornecedor entity.
     *
     * @Route("/new", name="fornecedor_new")
     * @Method({"GET", "POST"})
     * @Template("NewWolfEstoqueBundle::Fornecedor/form.html.twig")
     */
    public function newAction(Request $request)
    {
        return $this->salvarFornecedor($request, new Fornecedor);
    }

    /**
     *
     * @Route("/{id}/edit", name="fornecedor_edit")
     * @Method({"GET", "POST"})
     * @Template("NewWolfEstoqueBundle::Fornecedor/form.html.twig")
     */
    public function editAction(Request $request, Fornecedor $fornecedor)
    {
        return $this->salvarFornecedor($request, $fornecedor);
    }

    private function salvarFornecedor(Request $request, Fornecedor $fornecedor)
    {
        $form = $this->createForm('NewWolf\EstoqueBundle\Form\FornecedorType', $fornecedor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fornecedor->setUsuario($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($fornecedor);
            $em->flush();

            return $this->redirectToRoute('fornecedor_index', array('id' => $fornecedor->getId()));
        }

        return array(
            'fornecedor' => $fornecedor,
            'form' => $form->createView(),
        );
    }

    
    
    
    
}
